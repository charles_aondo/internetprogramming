<?php
    session_start();
?>
<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Trivia Questions</title>
    <link rel="stylesheet" href="bootstrap-3.3.7-dist/css/bootstrap.css">
    <link rel="stylesheet" href="bootstrap-3.3.7-dist/css/triviaStyleSheet.css">
    <script src="bootstrap-3.3.7-dist/js/bootstrap.min.js"></script>
</head>
<body>
    <div id="container">
        <?php
        //Restarting the game and clearing the session variables
        if(isset($_GET['action'])){
            if($_GET['action'] == 'restartGame') {
                session_destroy();
//                 kills client-side cookie that stores
//                 the session id.
                if (ini_get("session.use_cookies")) {
                    $params = session_get_cookie_params();
                    setcookie(session_name(), '', time() - 42000,
                        $params["path"], $params["domain"],
                        $params["secure"], $params["httponly"]
                    );
                }
            }
            header('location:trivia.php');
        }
        //Checking if its the users first time of visiting the page
        if(isset($_POST['submitQuiz'])){
            //Validating users input to make sure it is not empty
            if(!empty($_POST['userAnswers'])){
                //Converting the users answer to numberOfCorrectQuestions lowercase
                $userAnswers=strtolower($_POST["userAnswers"]);
                if(!isset($_SESSION['userAnswers'])){
                    $_SESSION['userAnswers']=[];
                }
                //Storing users answers in an array
                if(isset($_POST["userAnswers"])){
                    array_push($_SESSION['userAnswers'],$userAnswers);
                }
                //Counting number of times a user has entered an answer
                $_SESSION['counter']++;
            }else{
                echo '<h3 class="error">Please Provide an Answer! Field cannot be Empty</h3>';
            }
            //Using cookies to change the theme of the page
        }else if(isset($_POST['choseLayout'])){
            $selectLayout = $_POST['choseLayout'];
            setcookie('choseLayout','$selectLayout',time()+60*60*60);
            if($selectLayout == 1){
                ?>
                <style>
                    body {background-color: red;}
                    td   {color: blue;}
                    tr    {color: red;}
                    {
                        font-size:x-large;
                    ;}
                </style>
        <?php
            }if($selectLayout == 2){
                ?>
                <style>
                    body {background-color: black;}
                    td   {color: blue;}
                    tr    {color: red;}
                    {
                        font-size:x-large;
                    ;}
                </style>
                <?php
            }if($selectLayout==3){
                ?>
                <style>
                    body {background-color: greenyellow;}
                    td   {color: blue;}
                    tr    {color: red;}
                    {
                        font-size:x-large;
                    ;}
                </style>
                <?php
            }

        }else if(isset($_COOKIE['choseLayout'])){
            $selectLayout = $_COOKIE['choseLayout'];
        }
        else{
            //File path
            $filePath = 'triviaQuestions.txt';
            //Variables that store the array
            $_SESSION['counter']=0;
            //Creating an array to store the user answers
            $triviaArrayQuestions = [];
            $triviaAnswers = [];
            //Validating file path
            if (filesize($filePath) == 0 && file_exists($filePath)) {
                echo "<h1>Error!!</h1>" . " " . "<numberOfCorrectQuestions href='trivia.php'>Refresh page</numberOfCorrectQuestions>";
            } else {
                //Creating numberOfCorrectQuestions file array
                $fileArray = file($filePath);
                foreach ($fileArray as $triviaQuestions) {
                    $triviaQuestionPieces = explode("\t", $triviaQuestions);
                    //Making an array of arrays to store the questions and answers
                    array_push($triviaArrayQuestions,$triviaQuestionPieces[0]);
                    array_push($triviaAnswers,$triviaQuestionPieces[1]);
                    //Storing the questions and answers in numberOfCorrectQuestions session array
                    $_SESSION['sessionQuestions'] =$triviaArrayQuestions;
                    //Converting the answers from the file to lower case letters for comparison
                    $_SESSION['sessionAnswers'] = array_map('strtolower',$triviaAnswers);
                }
            }
        }
        ?>
       <?php
       $count=0;
       $count1=0;
       $count2=0;
       if($_SESSION['counter']==sizeof($_SESSION['sessionQuestions'])){
           $percentageCount = $_SESSION['correctAnswersCount']/count($_SESSION['sessionQuestions'])*100;
           echo "Percentage Score: ".round($percentageCount)."%";
           echo "<table class='table table-bordered table-striped'>";
           echo "<tr>";
           echo "<td>Number of Question</td>";
           echo "<td>The Question</td>";
           echo "<td>Your Answers</td>";
           echo "<td>Correct Answers</td>";
           echo "</tr>";
           foreach($_SESSION['sessionAnswers']as $Answer ){
               if(trim($Answer)==trim($_SESSION['userAnswers'][$count])){
                 echo  " <tr style='background-color: green'> ";
               }else{
                   echo  " <tr style='background-color: red'>  ";
               }
               echo "<td>";
               echo $count2+=1;
               echo "</td>";
               echo "<td>".$_SESSION['sessionQuestions'][ $count1++]."</td>";
               echo "<td>".$_SESSION['userAnswers'][$count++]."</td>";
               echo "<td>$Answer</td>";
               echo "</tr>";
           }
           echo "</table>";
           ?>
            <a href="trivia.php?action=restartGame">Restart</a></h2><br>
           <?php
       }else {
           ?>
<!--           Displaying the questions and a text book for the user to enter their answer-->
           <form method="POST" action="trivia.php">
                <div class="form-group">
                    <label><?php
                        echo $_SESSION['sessionQuestions'][$_SESSION['counter']];
                        ?>
                    </label>
                    <input type="text" name="userAnswers" class="form-control">
                </div>
               <button type="submit" name="submitQuiz" class="btn btn-primary">Next</button>
           </form>
<!--           Link that restarts the game-->
         <h2><a href="trivia.php?action=restartGame">Restart</a></h2><br>
<!--           Providing an interface through a form for the user to select and set their theme-->
           <form method="post" action="trivia.php">
               <div class="form-group">
                   <select name="choseLayout">
                       <option value="1"<?php echo ($selectLayout = 1 ? "selected":"");?>>Visual impairment</option>
                       <option value="2"<?php echo ($selectLayout = 2 ? "selected":"");?>>High Contrast</option>
                       <option value="3" <?php echo ($selectLayout = 3 ? "selected":"");?>>Taystee</option>
                   </select>
               </div>
               <button type="submit" name="set" class="btn btn-primary">Set Theme</button>
           </form>
           <?php
//looping through the session array to get number of questions the user got right and wrong
           $numberOfQuestionsAnswered =0;
           $_SESSION['correctAnswersCount'] = 0;
           while ($numberOfQuestionsAnswered < $_SESSION['counter']) {
               if ($_SESSION['userAnswers'][$numberOfQuestionsAnswered] == trim($_SESSION['sessionAnswers'][$numberOfQuestionsAnswered])) {
                   //Counting and storing the questions the user got right in a session array
                   $_SESSION['correctAnswersCount']++;
               } else {
                   if ($numberOfQuestionsAnswered == $_SESSION['counter'] - 1) {
                       echo '<h3 class="error">Wrong Answer! Better Luck Next Time</h3>'.".<br>";
                   }
               }
               $numberOfQuestionsAnswered++;
           }
           echo '<h4>Question Answered:</h4> '. $numberOfQuestionsAnswered ." of " . count($_SESSION['sessionQuestions']);
       }
       ?>
    </div>
</body>
</html>